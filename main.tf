provider "aws" {
    region = "us-east-1"
}

module "instance" {
    source                  = "git::https://gitlab.com/ricardocg-tf-aws/tf-ec2.git?ref=master"
    instance_count          = 1
    name                    = "my-test-vm"
    ami                     = "ami-09d95fab7fff3776c"
    instance_type           = "t2.large"
    key_name                = "key-mac"
}
